resource "aws_ecr_repository" "lfw_welcome" {
  name = "lfw_welcome"
}

resource "aws_ecr_repository_policy" "lfw_welcome" {
  repository = "${aws_ecr_repository.lfw_welcome.name}"

  policy = "${file("policies/ecr_policy.json")}"
}
