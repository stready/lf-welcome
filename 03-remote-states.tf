data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "lfw-terraform"
    key    = "${var.envname}/${var.aws_region}/lfw-vpc/terraform.tfstate"
    region = "${var.aws_region}"
  }
}
